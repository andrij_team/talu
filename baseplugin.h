#ifndef BASEPLUGIN
#define BASEPLUGIN

#include <QtCore/QObject>
#include <QtCore/QPluginLoader>
#include <QtCore/QString>
#include <QtCore/QtPlugin>
#include <QtCore/QUrl>

#include <QtQml/QQmlEngine>
#include <QtQuick/QQuickItem>

class BasePlugin : public QObject
{
    Q_OBJECT
public:
    virtual ~BasePlugin(){}
    virtual void registrQmlProperty() = 0;
    virtual QString getName() = 0;

    virtual bool isAboutPresent()      { return false; }
    virtual bool isLocalPresent()      { return false; }
    virtual bool isNetworkPresent()    { return false; }
    virtual bool isSaveModePresent()   { return false; }
    virtual bool isSettingsPresent()   { return false; }
    virtual bool isOnePlayerPresent()  { return false; }
    virtual bool isCoopPlayerPresent() { return false; }

    virtual void closeApp() = 0;
    virtual QQuickItem* createGameItem(QQmlEngine *engine) = 0;
    virtual QQuickItem* createAboutItem(QQmlEngine *engine)    { Q_UNUSED(engine); return nullptr; }
    virtual QQuickItem* createSettingsItem(QQmlEngine *engine) { Q_UNUSED(engine); return nullptr; }

signals:
    std::pair <bool, QString>   saveData(QString title, QByteArray data);
    std::pair <bool, QByteArray> getData(QString title);
    void setContextProperty(QString name, QObject *obj);
    QSize getMenuSize();
};

#define BasePlugin_iid "org.talu.baseplugin"
Q_DECLARE_INTERFACE(BasePlugin, BasePlugin_iid)

#endif
