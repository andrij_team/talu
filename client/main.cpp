#include <QtGui/QGuiApplication>
#include "C++/talumanager.h"

int main(int argc, char **argv){
    QGuiApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/client/image/icon.jpg"));

    TaluManager *talu = new TaluManager;
    talu->start();

    return app.exec();
}
