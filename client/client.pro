QT += qml
QT += quick
QT += network

CONFIG += c++14
CONFIG += qtquickcompiler
DESTDIR = $$PWD/../../TaLu_Bin
TARGET = TaLu

HEADERS += \
    C++/baseplugin.h \
    C++/talumanager.h \
    C++/talu.h

SOURCES += \
    main.cpp \
    C++/talumanager.cpp

OTHER_FILES += $$PWD/qml/*.qml

RESOURCES += \
    resourse.qrc
