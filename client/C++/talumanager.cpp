#include "talumanager.h"

TaluManager::TaluManager(QObject *parent) : QObject(parent), isModuleOpen(false)
{
    view = new QQuickView(QUrl("qrc:/client/qml/main.qml"));
    moduleRect = view->rootObject()->findChild <QQuickItem*> ("contentRect");
    qDebug() << moduleRect;
    view->rootContext()->setContextProperty("Qt_main", this);
    QMetaObject::invokeMethod(view->rootObject(), "setHomeStatus");
}

void TaluManager::brutalCloseModule()
{
    QMetaObject::invokeMethod(view->rootObject(), "setHomeStatus");
    isModuleOpen = false;
}

void TaluManager::safelyCloseModule()
{
    //
}

void TaluManager::start()
{
    view->resize(300, 500);
    view->show();
    QDir dir(QDir::current());
    if (dir.cd("modules")){
    #ifdef Q_OS_LINUX
        dir.setNameFilters(QStringList{"*.so"});
    #elif Q_OS_WIN
        dir.setNameFilters(QStringList{"*.dll"});
    #endif
        QStringList list = dir.entryList();
        for (auto m : list)
            connectPlugin(TaLu::findModuleName(m));
    }
}

bool TaluManager::connectPlugin(QString pluginName)
{
    QDir dir(QDir::current());
    dir.cd("modules");
    QPluginLoader loader(this);

#ifdef Q_OS_LINUX
    loader.setFileName(dir.absoluteFilePath("lib" + pluginName + ".so"));
#elif Q_OS_WIN
    loader.setFileName(dir.absoluteFilePath(pluginName + ".dll"));
#endif

    QObject *plugin = loader.instance();
    if (plugin){
        BasePlugin *currentPlugin = qobject_cast <BasePlugin*> (plugin);
        pluginList.push_back(currentPlugin);
        QObject::connect(currentPlugin, &BasePlugin::setContextProperty,
                         [=](QString str, QObject *obj)
        { view->rootContext()->setContextProperty(str, obj); qDebug() << str; });
        QObject::connect(currentPlugin, &BasePlugin::getMenuSize,
                         [=]()
        {
            auto item = view->rootObject()->findChild <QQuickItem*> ("topRect");
            qDebug() << item << item->height() << item->width();
            return QSize(item->width(), item->height());
        });
        currentPlugin->registrQmlProperty();
        qDebug() << "module " << pluginName << " loaded";
        return true;
    }
    qDebug() << "module " << pluginName << " not loaded cause " << loader.errorString();
    return false;
}

void TaluManager::quit()
{
    for (auto p : pluginList)
        p->closeModule();
    QGuiApplication::quit();
}

void TaluManager::getModuleListByString(QString str)
{
    QStringList sendList;
    if (str == "one"){
        for (auto p : pluginList)
            if (p->isOnePlayerPresent())
                sendList.push_back(p->getName());
    }
    else if (str == "coop"){
        for (auto p : pluginList)
            if (p->isCoopPlayerPresent())
                sendList.push_back(p->getName());
    }
    else if (str == "local"){
        for (auto p : pluginList)
            if (p->isLocalPresent())
                sendList.push_back(p->getName());
    }
    else if (str == "network"){
        for (auto p : pluginList)
            if (p->isNetworkPresent())
                sendList.push_back(p->getName());
    }
    QMetaObject::invokeMethod(view->rootObject()->findChild <QObject*> ("listOfChoose"),
                              "setButtons", Q_ARG(QVariant, str),
                              Q_ARG(QVariant, QVariant::fromValue(sendList)));
}

void TaluManager::choosePlugin(QString name, QString type)
{
    for (auto p : pluginList)
        if (p->getName() == name){
            // set type
            qDebug() << moduleRect;
            isModuleOpen = true;
            p->createGameItem(view->engine())->setParentItem(moduleRect);
            QMetaObject::invokeMethod(view->rootObject(), "setTitleText", Q_ARG(QVariant, p->getName()));
        }
}
