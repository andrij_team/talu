#ifndef TALU_H
#define TALU_H

#include <QtCore>

class TaLu : public QObject
{
    Q_OBJECT
public:
    enum class GameType {
        Solo         = 0,
        Cooperative  = 1,
        LocalNetwork = 2,
        Internet     = 3,
    };
    Q_ENUMS(GameType)

    enum class Version {
        V_0_1 = 0,
    };
    Q_ENUMS(Version)

    static QString findModuleName(QString fileName)
    {
        QString ret("");
    #ifdef Q_OS_LINUX
        ret = fileName.mid(3, fileName.length() - 6);
    #elif Q_OS_WIN
        ret = fileName.left(fileName.length() - 4);
    #endif
        return ret;
    }
};

#endif // TALU_H
