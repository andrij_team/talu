#ifndef TALUMANAGER_H
#define TALUMANAGER_H

#include <QtCore>
#include <QtQml>
#include <QtQuick>
#include <QGuiApplication>

#include "talu.h"
#include "baseplugin.h"

class TaluManager : public QObject
{
    Q_OBJECT
private:
    QList <BasePlugin*> pluginList;
    QQuickItem *moduleRect;
    QQuickView *view;
    bool isModuleOpen;

    bool connectPlugin(QString pluginName);
    QString findModuleName(QString fileName);

public:
    TaluManager(QObject *parent = nullptr);
    void start();

    Q_INVOKABLE void quit();
    Q_INVOKABLE void getModuleListByString(QString str);
    Q_INVOKABLE void choosePlugin(QString name, QString type);
    Q_INVOKABLE bool moduleOpenStatus(){ return isModuleOpen; }
    Q_INVOKABLE void brutalCloseModule();
    Q_INVOKABLE void safelyCloseModule();
};

#endif // TALUMANAGER_H
