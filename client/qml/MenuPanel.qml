import QtQuick 2.0

Rectangle {
    opacity: 0.9

    Rectangle {
        id: onePlayerButton
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: buttonSize

        Image {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: buttonSize
            source: "qrc:/client/image/player_vs_pc.jpg"
        }

        Text {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width - buttonSize
            text: "Player vs AI"
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                hideAllContent();
                listOfChoose.visible = true;
                Qt_main.getModuleListByString("one");
                setTitleText("One player games");
            }
        }
    }

    Rectangle {
        id: dualPlayerButton
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: onePlayerButton.bottom
        height: buttonSize

        Image {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: buttonSize
            source: "qrc:/client/image/player_vs_player.jpg"
        }

        Text {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width - buttonSize
            text: "Player vs Player"
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                hideAllContent();
                listOfChoose.visible = true;
                Qt_main.getModuleListByString("coop");
                setTitleText("Cooperators games");
            }
        }
    }

    Rectangle {
        id: localPlayerButton
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: dualPlayerButton.bottom
        height: buttonSize

        Image {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: buttonSize
            source: "qrc:/client/image/local_game.ico"
        }

        Text {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width - buttonSize
            text: "Local game"
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                hideAllContent();
                listOfChoose.visible = true;
                Qt_main.getModuleListByString("local");
                setTitleText("Local network games");
            }
        }
    }

    Rectangle {
        id: networkPlayerButton
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: localPlayerButton.bottom
        height: buttonSize

        Image {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: buttonSize
            source: "qrc:/client/image/internet.png"
        }

        Text {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width - buttonSize
            text: "Network game"
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                hideAllContent();
                listOfChoose.visible = true;
                Qt_main.getModuleListByString("network");
                setTitleText("Internet games");
            }
        }
    }

    Rectangle {
        id: accountButton
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: settingsButton.top
        height: buttonSize

        Image {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: buttonSize
            source: "qrc:/client/image/account.png"
        }

        Text {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width - buttonSize
            text: "Account"
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                hideAllContent();
                accountRect.visible = true;
                // ?????
                setTitleText("Account");
            }
        }
    }

    Rectangle {
        id: settingsButton
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: aboutButton.top
        height: buttonSize

        Image {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: buttonSize
            source: "qrc:/client/image/settings.jpg"
        }

        Text {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width - buttonSize
            text: "Settings"
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                hideAllContent();
                settingRect.visible = true;
                setTitleText("Settings");
            }
        }
    }

    Rectangle {
        id: aboutButton
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: exitButton.top
        height: buttonSize

        Image {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: buttonSize
            source: "qrc:/client/image/about.png"
        }

        Text {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width - buttonSize
            text: "About"
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                hideAllContent();
                aboutRect.visible = true;
                setTitleText("About");
            }
        }
    }

    Rectangle {
        id: exitButton
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: buttonSize

        Image {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: buttonSize
            source: "qrc:/client/image/power.png"
        }

        Text {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: parent.width - buttonSize
            text: "Exit application"
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: Qt_main.quit();
        }
    }
}
