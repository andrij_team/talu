import QtQuick 2.0

Rectangle {
    anchors.fill: parent
    opacity: 0.9
    color: darkMenuColor

    MouseArea {
        anchors.fill: parent
        onClicked: closeWarningRect.visible = false;
    }

    Rectangle {
        anchors.centerIn: parent
        color: menuColor
        height: parent.height / 7
        width: parent.width * 2 / 3

        Text {
            anchors.centerIn: parent
            text: "Close game"
            color: textColor
            font.pointSize: parent.height / 4
        }

        MouseArea {
            anchors.fill: parent
            onClicked: Qt_main.brutalCloseModule();
        }
    }
}
