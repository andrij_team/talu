import QtQuick 2.0
import "qrc:/client/qml"

Item {
    id: mainWindow
    anchors.fill: parent

    property string menuColor: "#0000ff"
    property string textColor: "white"
    property string darkMenuColor: "#0000d0"
    property int buttonSize: height / 11

    function setHomeStatus(){
        hideAllContent();
        menuPanel.visible = true;
    }

    function setTitleText(titleText){
        topRectText.text = titleText;
    }

    function hideAllContent(){
        menuPanel.visible = false;
        listOfChoose.visible = false;
        contentRect.visible = false;
        accountRect.visible = false;
        aboutRect.visible = false;
        settingRect.visible = false;
        closeWarningRect.visible = false;
    }

    Rectangle {
        id: topRect
        objectName: "topRect"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: buttonSize
        color: menuColor

        Text {
            id: topRectText
            anchors.centerIn: parent
            font.pointSize: parent.height / 4
            color: textColor
        }

        MenuButton {
            id: menuButton
            anchors.top: parent.top
            anchors.left: parent.left
            height: buttonSize
            width: buttonSize
            color: darkMenuColor

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (Qt_main.moduleOpenStatus())
                        closeWarningRect.visible = true;
                    else {
                        hideAllContent();
                        menuPanel.visible = true;
                    }
                }
            }
        }
    }

    Rectangle {
        anchors.top: topRect.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        ListOfChoose {
            id: listOfChoose
            objectName: "listOfChoose"
            anchors.fill: parent
        }

        Item {
            id: contentRect
            objectName: "contentRect"
            anchors.fill: parent
        }

        MenuPanel {
            id: menuPanel
            anchors.fill: parent

            onVisibleChanged: {
                menuButton.visible = ! menuPanel.visible;
                setTitleText("Ta. Lu. menu");
            }
        }

        About {
            id: aboutRect
            anchors.fill: parent
        }

        Account {
            id: accountRect
            anchors.fill: parent
        }

        Setting {
            id: settingRect
            anchors.fill: parent
        }

        CloseModuleWarning {
            id: closeWarningRect
            anchors.fill: parent
        }
    }
}
