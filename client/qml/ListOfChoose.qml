import QtQuick 2.5
import QtQuick.Controls 2.0

ListView {
    id: listView
    property string type: ""

    function setButtons(type, list){
        listChooseModel.clear();
        listView.type = type;
        for (var i = 0; i < list.length; i++)
            listChooseModel.append({ buttonText: list[i] });
    }

    delegate: Item {
        id: item
        anchors.left: parent.left
        anchors.right: parent.right
        height: buttonSize

        Rectangle {
            anchors.fill: parent
            color: "orange"
            border.color: "black"

            Text {
                anchors.centerIn: parent
                text: buttonText
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    Qt_main.choosePlugin(buttonText, type);
                    hideAllContent();
                    contentRect.visible = true;
                }
            }
        }
    }

    model: ListModel {
        id: listChooseModel
    }
}
