import QtQuick 2.5

Rectangle {
    color: menuColor

    onVisibleChanged: canvasMenuButton.requestPaint();

    Item {
        id: itemMenuButton
        anchors.centerIn: parent
        height: parent.height * 0.4
        width: parent.width * 0.6

        Canvas {
            id: canvasMenuButton
            anchors.fill: parent

            onPaint: {
                var context = getContext("2d");
                var lineWidth = itemMenuButton.height / 5
                context.beginPath();
                context.lineWidth = lineWidth
                context.strokeStyle = "white"

                context.moveTo(0, lineWidth / 2);
                context.lineTo(itemMenuButton.width, lineWidth / 2);

                context.moveTo(0, itemMenuButton.height / 2);
                context.lineTo(itemMenuButton.width, itemMenuButton.height / 2);

                context.moveTo(0, itemMenuButton.height - lineWidth / 2);
                context.lineTo(itemMenuButton.width, itemMenuButton.height - lineWidth / 2);

                context.stroke();
                console.log(context.lineWidth, itemMenuButton.width, itemMenuButton.height);
            }
        }
    }
}
