#ifndef PLAYER_H
#define PLAYER_H

#include <QtCore>

class PlayerInfo : public QObject
{
    Q_OBJECT
private:
    Q_PROPERTY(QString name  READ name)
    Q_PROPERTY(QString color READ color)
    Q_PROPERTY(int score READ score WRITE setScore NOTIFY scoreChanged)

    QString m_name;
    QString m_color;
    int m_score;

public:
    PlayerInfo() = default;
    PlayerInfo(QString name, QString color)
        : m_name(name), m_color(color) {}
    QString name()  const { return m_name;  }
    QString color() const { return m_color; }
    int     score() const { return m_score; }

    void setScore(int score){
        m_score = score;
        emit scoreChanged(score);
    }

signals:
    void scoreChanged(int score);
};

#endif // PLAYER_H
