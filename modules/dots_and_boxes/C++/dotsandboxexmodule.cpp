#include "dotsandboxexmodule.h"

DotsAndBoxexModule::DotsAndBoxexModule()
{

}

void DotsAndBoxexModule::registrQmlProperty(){}

void DotsAndBoxexModule::closeModule(){}

QString DotsAndBoxexModule::getName()
{
    return "Dotes and Boxes";
}

QQuickItem* DotsAndBoxexModule::createGameItem(QQmlEngine *engine)
{
    QQmlComponent component(engine, QUrl("qrc:/d_and_b/QML/main.qml"));
    qDebug() << component.errors() << component.isReady();
    return qobject_cast <QQuickItem*> (component.create());
}
