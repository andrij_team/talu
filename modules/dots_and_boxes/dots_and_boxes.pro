QT += qml
QT += quick

CONFIG += c++14
CONFIG += plugin
CONFIG += qtquickcompiler
DESTDIR = $$PWD/../../../TaLu_Bin/modules
TARGET = dots_and_boxes
TEMPLATE = lib

OTHER_FILES += $$PWD/QML/*.qml

HEADERS += \
    C++/dotsandboxexmodule.h \
    C++/player.h

SOURCES += \
    C++/dotsandboxexmodule.cpp

RESOURCES += \
    d_and_b_resource.qrc
