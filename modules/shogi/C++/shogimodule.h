#ifndef SHOGIMODULE_H
#define SHOGIMODULE_H

#include <QtCore/QObject>
#include <QtCore/QPluginLoader>
#include <QtCore/QString>
#include <QtCore/QtPlugin>
#include <QtCore/QUrl>

#include <QtQml/QQmlEngine>
#include <QtQuick/QQuickItem>

class BasePlugin : public QObject
{
    Q_OBJECT
public:
    virtual ~BasePlugin(){}
    virtual void registrQmlProperty() = 0;
    virtual QString getName() = 0;

    virtual bool isAboutPresent()      { return false; }
    virtual bool isLocalPresent()      { return false; }
    virtual bool isNetworkPresent()    { return false; }
    virtual bool isSaveModePresent()   { return false; }
    virtual bool isSettingsPresent()   { return false; }
    virtual bool isOnePlayerPresent()  { return false; }
    virtual bool isCoopPlayerPresent() { return false; }

    virtual void closeModule() = 0;
    virtual QQuickItem* createGameItem(QQmlEngine *engine) = 0;
    virtual QQuickItem* createAboutItem(QQmlEngine *engine)    { Q_UNUSED(engine); return nullptr; }
    virtual QQuickItem* createSettingsItem(QQmlEngine *engine) { Q_UNUSED(engine); return nullptr; }

signals:
    std::pair <bool, QString>   saveData(QString title, QByteArray data);
    std::pair <bool, QByteArray> getData(QString title);
    void setContextProperty(QString name, QObject *obj);
    QSize getMenuSize();
};

#define BasePlugin_iid "org.talu.baseplugin"
Q_DECLARE_INTERFACE(BasePlugin, BasePlugin_iid)

class ShogiModule : public BasePlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.felenko.andrey.talu.baseplugin")
    Q_INTERFACES(BasePlugin)
public:
    ShogiModule();

    virtual void registrQmlProperty() Q_DECL_OVERRIDE;
    virtual QString getName() Q_DECL_OVERRIDE;
    virtual void closeModule() Q_DECL_OVERRIDE;
    virtual QQuickItem* createGameItem(QQmlEngine *engine) Q_DECL_OVERRIDE;
};

#endif // SHOGIMODULE_H
