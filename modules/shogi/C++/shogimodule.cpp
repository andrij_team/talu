#include "shogimodule.h"

ShogiModule::ShogiModule()
{}

void ShogiModule::registrQmlProperty(){}

void ShogiModule::closeModule(){}

QString ShogiModule::getName()
{
    return "Shogi";
}

QQuickItem* ShogiModule::createGameItem(QQmlEngine *engine)
{
    QQmlComponent component(engine, QUrl("qrc:/shogi/main.qml"));
    qDebug() << component.errors() << component.isReady();
    return qobject_cast <QQuickItem*> (component.create());
}
