#include "renjumodule.h"

RenjuModule::RenjuModule()
{

}

void RenjuModule::registrQmlProperty(){}

void RenjuModule::closeModule(){}

QString RenjuModule::getName()
{
    return "Renju";
}

QQuickItem* RenjuModule::createGameItem(QQmlEngine *engine)
{
    QQmlComponent component(engine, QUrl("qrc:/renju/QML/main.qml"));
    qDebug() << component.errors() << component.isReady();
    return qobject_cast <QQuickItem*> (component.create());
}
