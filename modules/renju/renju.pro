QT += qml
QT += quick

CONFIG += c++14
CONFIG += plugin
CONFIG += qtquickcompiler
DESTDIR = $$PWD/../../../TaLu_Bin/modules
TARGET = renju
TEMPLATE = lib

OTHER_FILES += $$PWD/QML/*.qml

HEADERS += \
    C++/renjumodule.h

SOURCES += \
    C++/renjumodule.cpp

RESOURCES += \
    renju_resource.qrc
