QT += qml
QT += quick

CONFIG += c++14
CONFIG += plugin
CONFIG += qtquickcompiler
DESTDIR = $$PWD/../../../TaLu_Bin/modules
TARGET = go
TEMPLATE = lib

OTHER_FILES += $$PWD/QML/*.qml

SOURCES += \
    C++/gomodule.cpp

HEADERS += \
    C++/gomodule.h

RESOURCES += \
    go_resource.qrc
