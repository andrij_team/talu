#include "gomodule.h"

GoModule::GoModule(){}

void GoModule::closeModule(){}

void GoModule::registrQmlProperty(){}

bool GoModule::isLocalPresent()
{
    return false;
}

bool GoModule::isNetworkPresent()
{
    return false;
}

bool GoModule::isOnePlayerPresent()
{
    return false;
}

bool GoModule::isCoopPlayerPresent()
{
    return false;
}

QString GoModule::getName()
{
    return "Go";
}

QQuickItem* GoModule::createGameItem(QQmlEngine *engine)
{
    QQmlComponent component(engine, QUrl("qrc:/go/QML/main.qml"));
    qDebug() << component.errors() << component.isReady();
    return qobject_cast <QQuickItem*> (component.create());
}
