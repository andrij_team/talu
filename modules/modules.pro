TEMPLATE = subdirs

SUBDIRS += \
    go      \
    risk     \
    chess     \
    renju      \
    shogi       \
    xiangqi      \
    checkers      \
    dots_and_boxes \
    noughts_and_crosses
