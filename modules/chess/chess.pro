QT += qml
QT += quick

CONFIG += c++14
CONFIG += plugin
CONFIG += qtquickcompiler
DESTDIR = $$PWD/../../../TaLu_Bin/modules
TARGET = chess
TEMPLATE = lib

OTHER_FILES += $$PWD/QML/*.qml

SOURCES += \
    C++/chessmodule.cpp

HEADERS += \
    C++/chessmodule.h

RESOURCES += \
    chess_resource.qrc
