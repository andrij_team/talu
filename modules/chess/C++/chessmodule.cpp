#include "chessmodule.h"

ChessModule::ChessModule(){}

void ChessModule::closeModule(){}

void ChessModule::registrQmlProperty(){}

bool ChessModule::isLocalPresent()
{
    return true;
}

bool ChessModule::isNetworkPresent()
{
    return false;
}

bool ChessModule::isOnePlayerPresent()
{
    return false;
}

bool ChessModule::isCoopPlayerPresent()
{
    return false;
}

QString ChessModule::getName()
{
    return "Chess";
}

QQuickItem* ChessModule::createGameItem(QQmlEngine *engine)
{
    QQmlComponent component(engine, QUrl("qrc:/chess/QML/main.qml"));
    qDebug() << component.errors() << component.isReady();
    return qobject_cast <QQuickItem*> (component.create());
}
