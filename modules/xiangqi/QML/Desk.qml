import QtQuick 2.0
import Xiangqi 1.0

Rectangle {
    id: deskRect
    anchors.fill: parent
    color: "#bf4c19"

    property int widthSquare: 100
    property int shiftH: (height - widthSquare*9)/2
    property int shiftW: (width  - widthSquare*8)/2

    function resizeEvent(height, width){
        widthSquare = Math.min(height, width) / 10;
        redraw();
    }

    function setPossibleTurn(current, enemyFigure, enemyPath, playerPath){
        // clear last possible component
        clearTurnLayer();

        // set current figure
        if (current >= 0)
            Qt.createComponent("TurnCross.qml").createObject(turnLayer, { "position" : current,
                                                                          "border.color" : "green",
                                                                          "border.width" : 3 });
        // set enemy figure
        for (var i = 0; i < enemyFigure.length; i++)
            Qt.createComponent("TurnCross.qml").createObject(turnLayer, { "position" : enemyFigure[i],
                                                                          "border.color" : "red",
                                                                          "border.width" : 3 });

        // set enemy path
        for (i = 0; i < enemyPath.length; i++)
            Qt.createComponent("TurnCross.qml").createObject(turnLayer, { "position" : enemyPath[i],
                                                                          "color" : "red",
                                                                          "opacity" : 0.6 });

        // set player path
        for (i = 0; i < playerPath.length; i++)
            Qt.createComponent("TurnCross.qml").createObject(turnLayer, { "position" : playerPath[i],
                                                                          "color" : "green",
                                                                          "opacity" : 0.7 });

        redraw()
    }

    function clearTurnLayer(){
        var lastTurn = turnLayer.children;
        for (var i = 0; i < lastTurn.length; i++)
            lastTurn[i].destroy();
        redraw();
    }

    function setResultText(text){
        textResult.text = text;
        infoResultRect.visible = true;
    }

    Item {
        id: fieldLayer
        anchors.fill: parent

        Rectangle {
            id: orangeRect
            height: widthSquare*9
            width: widthSquare*8
            x: shiftW
            y: shiftH
            color: "#ffcc00"
        }

        Rectangle {
            id: riverRect
            height: widthSquare
            width: widthSquare*8
            x: shiftW
            y: shiftH + 4*widthSquare
            color: "navy"
        }

        Canvas {
            id: canvasField
            anchors.fill: parent

            onPaint: {
                var context = getContext("2d")
                context.beginPath();
                context.lineWidth = 2;
                context.strokeStyle = "black"

                //border
                context.moveTo(shiftW, shiftH);
                context.lineTo(shiftW + 8*widthSquare, shiftH);

                context.moveTo(shiftW + 8*widthSquare, shiftH);
                context.lineTo(shiftW + 8*widthSquare, shiftH + 9*widthSquare);

                context.moveTo(shiftW + 8*widthSquare, shiftH + 9*widthSquare);
                context.lineTo(shiftW, shiftH + 9*widthSquare);

                context.moveTo(shiftW, shiftH + 9*widthSquare);
                context.lineTo(shiftW, shiftH);

                //horizontal lines
                context.moveTo(shiftW, shiftH + widthSquare);
                context.lineTo(shiftW + 8*widthSquare, shiftH + widthSquare);
                context.moveTo(shiftW, shiftH + 2*widthSquare);
                context.lineTo(shiftW + 8*widthSquare, shiftH + 2*widthSquare);
                context.moveTo(shiftW, shiftH + 3*widthSquare);
                context.lineTo(shiftW + 8*widthSquare, shiftH + 3*widthSquare);
                context.moveTo(shiftW, shiftH + 4*widthSquare);
                context.lineTo(shiftW + 8*widthSquare, shiftH + 4*widthSquare);
                context.moveTo(shiftW, shiftH + 5*widthSquare);
                context.lineTo(shiftW + 8*widthSquare, shiftH + 5*widthSquare);
                context.moveTo(shiftW, shiftH + 6*widthSquare);
                context.lineTo(shiftW + 8*widthSquare, shiftH + 6*widthSquare);
                context.moveTo(shiftW, shiftH + 7*widthSquare);
                context.lineTo(shiftW + 8*widthSquare, shiftH + 7*widthSquare);
                context.moveTo(shiftW, shiftH + 8*widthSquare);
                context.lineTo(shiftW + 8*widthSquare, shiftH + 8*widthSquare);

                //up desk lines
                context.moveTo(shiftW + widthSquare, shiftH);
                context.lineTo(shiftW + widthSquare, shiftH + 4*widthSquare);
                context.moveTo(shiftW + 2*widthSquare, shiftH);
                context.lineTo(shiftW + 2*widthSquare, shiftH + 4*widthSquare);
                context.moveTo(shiftW + 3*widthSquare, shiftH);
                context.lineTo(shiftW + 3*widthSquare, shiftH + 4*widthSquare);
                context.moveTo(shiftW + 4*widthSquare, shiftH);
                context.lineTo(shiftW + 4*widthSquare, shiftH + 4*widthSquare);
                context.moveTo(shiftW + 5*widthSquare, shiftH);
                context.lineTo(shiftW + 5*widthSquare, shiftH + 4*widthSquare);
                context.moveTo(shiftW + 6*widthSquare, shiftH);
                context.lineTo(shiftW + 6*widthSquare, shiftH + 4*widthSquare);
                context.moveTo(shiftW + 7*widthSquare, shiftH);
                context.lineTo(shiftW + 7*widthSquare, shiftH + 4*widthSquare);

                //down desk lines
                context.moveTo(shiftW + widthSquare, shiftH + 9*widthSquare);
                context.lineTo(shiftW + widthSquare, shiftH + 5*widthSquare);
                context.moveTo(shiftW + 2*widthSquare, shiftH + 9*widthSquare);
                context.lineTo(shiftW + 2*widthSquare, shiftH + 5*widthSquare);
                context.moveTo(shiftW + 3*widthSquare, shiftH + 9*widthSquare);
                context.lineTo(shiftW + 3*widthSquare, shiftH + 5*widthSquare);
                context.moveTo(shiftW + 4*widthSquare, shiftH + 9*widthSquare);
                context.lineTo(shiftW + 4*widthSquare, shiftH + 5*widthSquare);
                context.moveTo(shiftW + 5*widthSquare, shiftH + 9*widthSquare);
                context.lineTo(shiftW + 5*widthSquare, shiftH + 5*widthSquare);
                context.moveTo(shiftW + 6*widthSquare, shiftH + 9*widthSquare);
                context.lineTo(shiftW + 6*widthSquare, shiftH + 5*widthSquare);
                context.moveTo(shiftW + 7*widthSquare, shiftH + 9*widthSquare);
                context.lineTo(shiftW + 7*widthSquare, shiftH + 5*widthSquare);

                //draw
                context.stroke();

                context.lineWidth = 1.5;
                //up castle
                context.moveTo(shiftW + 3*widthSquare, shiftH);
                context.lineTo(shiftW + 5*widthSquare, shiftH + 2*widthSquare);
                context.moveTo(shiftW + 3*widthSquare, shiftH + 2*widthSquare);
                context.lineTo(shiftW + 5*widthSquare, shiftH);

                //down castle
                context.moveTo(shiftW + 3*widthSquare, shiftH + 7*widthSquare);
                context.lineTo(shiftW + 5*widthSquare, shiftH + 9*widthSquare);
                context.moveTo(shiftW + 3*widthSquare, shiftH + 9*widthSquare);
                context.lineTo(shiftW + 5*widthSquare, shiftH + 7*widthSquare);

                //draw
                context.stroke();
            }
        }
    }

    Item {
        id: turnLayer
        anchors.fill: parent
    }

    Item {
        id: figureLayer
        anchors.fill: parent
        //figure
        Image {
            id: redGeneral
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.General, 0)
        }
        Image {
            id: redAdvisor0
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Advisor, 0)
        }
        Image {
            id: redAdvisor1
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Advisor, 1)
        }
        Image {
            id: redCannon0
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Cannon, 0)
        }
        Image {
            id: redCannon1
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Cannon, 1)
        }
        Image {
            id: redChariot0
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Chariot, 0)
        }
        Image {
            id: redChariot1
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Chariot, 1)
        }
        Image {
            id: redElephant0
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Elephant, 0)
        }
        Image {
            id: redElephant1
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Elephant, 1)
        }
        Image {
            id: redHorse0
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Horse, 0)
        }
        Image {
            id: redHorse1
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Horse, 1)
        }
        Image {
            id: redSoldier0
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Soldier, 0)
        }
        Image {
            id: redSoldier1
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Soldier, 1)
        }
        Image {
            id: redSoldier2
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Soldier, 2)
        }
        Image {
            id: redSoldier3
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Soldier, 3)
        }
        Image {
            id: redSoldier4
            source: Qt_Xiangqi.getImagePath("red", Xiangqi.Soldier, 4)
        }

        // white
        Image {
            id: whiteGeneral
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.General, 0)
        }
        Image {
            id: whiteAdvisor0
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Advisor, 0)
        }
        Image {
            id: whiteAdvisor1
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Advisor, 1)
        }
        Image {
            id: whiteCannon0
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Cannon, 0)
        }
        Image {
            id: whiteCannon1
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Cannon, 1)
        }
        Image {
            id: whiteChariot0
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Chariot, 0)
        }
        Image {
            id: whiteChariot1
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Chariot, 1)
        }
        Image {
            id: whiteElephant0
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Elephant, 0)
        }
        Image {
            id: whiteElephant1
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Elephant, 1)
        }
        Image {
            id: whiteHorse0
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Horse, 0)
        }
        Image {
            id: whiteHorse1
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Horse, 1)
        }
        Image {
            id: whiteSoldier0
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Soldier, 0)
        }
        Image {
            id: whiteSoldier1
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Soldier, 1)
        }
        Image {
            id: whiteSoldier2
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Soldier, 2)
        }
        Image {
            id: whiteSoldier3
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Soldier, 3)
        }
        Image {
            id: whiteSoldier4
            source: Qt_Xiangqi.getImagePath("white", Xiangqi.Soldier, 4)
        }
    }

    Rectangle {
        id: infoResultRect
        anchors.fill: parent
        opacity: 0.65
        color: "navy"
        Text {
            id: textResult
            anchors.centerIn: parent
            color: "#ffffff"
            font.family: "Purisa"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: if (infoResultRect.visible == false){
            Qt_match.clickToDesk((mouseX - shiftW + 0.5 * widthSquare) / widthSquare,
                                 (mouseY - shiftH + 0.5 * widthSquare) / widthSquare);
            redraw();
        }
    }

    onHeightChanged : resizeEvent(height, width);
    onWidthChanged  : resizeEvent(height, width);
    onVisibleChanged: { redraw(); infoResultRect.visible = false; }

    function redraw(){
        canvasField.requestPaint()

        // redraw turn cross
        var lastTurn = turnLayer.children;
        for (var i = 0; i < lastTurn.length; i++)
            lastTurn[i].redraw();

        // redGeneral
        redGeneral.height = widthSquare * 0.9
        redGeneral.width = widthSquare * 0.9
        redGeneral.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.General, 0) * widthSquare - 0.45 * widthSquare
        redGeneral.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.General, 0) * widthSquare - 0.45 * widthSquare
        // redAdvisor0
        redAdvisor0.height = widthSquare * 0.9
        redAdvisor0.width = widthSquare * 0.9
        redAdvisor0.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Advisor, 0) * widthSquare - 0.45 * widthSquare
        redAdvisor0.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Advisor, 0) * widthSquare - 0.45 * widthSquare
        // redAdvisor1
        redAdvisor1.height = widthSquare * 0.9
        redAdvisor1.width = widthSquare * 0.9
        redAdvisor1.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Advisor, 1) * widthSquare - 0.45 * widthSquare
        redAdvisor1.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Advisor, 1) * widthSquare - 0.45 * widthSquare
        // redCannon0
        redCannon0.height = widthSquare * 0.9
        redCannon0.width = widthSquare * 0.9
        redCannon0.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Cannon, 0) * widthSquare - 0.45 * widthSquare
        redCannon0.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Cannon, 0) * widthSquare - 0.45 * widthSquare
        // redCannon1
        redCannon1.height = widthSquare * 0.9
        redCannon1.width = widthSquare * 0.9
        redCannon1.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Cannon, 1) * widthSquare - 0.45 * widthSquare
        redCannon1.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Cannon, 1) * widthSquare - 0.45 * widthSquare
        // redChariot0
        redChariot0.height = widthSquare * 0.9
        redChariot0.width = widthSquare * 0.9
        redChariot0.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Chariot, 0) * widthSquare - 0.45 * widthSquare
        redChariot0.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Chariot, 0) * widthSquare - 0.45 * widthSquare
        // redChariot1
        redChariot1.height = widthSquare * 0.9
        redChariot1.width = widthSquare * 0.9
        redChariot1.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Chariot, 1) * widthSquare - 0.45 * widthSquare
        redChariot1.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Chariot, 1) * widthSquare - 0.45 * widthSquare
        // redElephant0
        redElephant0.height = widthSquare * 0.9
        redElephant0.width = widthSquare * 0.9
        redElephant0.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Elephant, 0) * widthSquare - 0.45 * widthSquare
        redElephant0.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Elephant, 0) * widthSquare - 0.45 * widthSquare
        // redElephant1
        redElephant1.height = widthSquare * 0.9
        redElephant1.width = widthSquare * 0.9
        redElephant1.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Elephant, 1) * widthSquare - 0.45 * widthSquare
        redElephant1.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Elephant, 1) * widthSquare - 0.45 * widthSquare
        // redHorse0
        redHorse0.height = widthSquare * 0.9
        redHorse0.width = widthSquare * 0.9
        redHorse0.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Horse, 0) * widthSquare - 0.45 * widthSquare
        redHorse0.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Horse, 0) * widthSquare - 0.45 * widthSquare
        // redHorse1
        redHorse1.height = widthSquare * 0.9
        redHorse1.width = widthSquare * 0.9
        redHorse1.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Horse, 1) * widthSquare - 0.45 * widthSquare
        redHorse1.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Horse, 1) * widthSquare - 0.45 * widthSquare
        // redSoldier0
        redSoldier0.height = widthSquare * 0.9
        redSoldier0.width = widthSquare * 0.9
        redSoldier0.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Soldier, 0) * widthSquare - 0.45 * widthSquare
        redSoldier0.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Soldier, 0) * widthSquare - 0.45 * widthSquare
        // redSoldier1
        redSoldier1.height = widthSquare * 0.9
        redSoldier1.width = widthSquare * 0.9
        redSoldier1.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Soldier, 1) * widthSquare - 0.45 * widthSquare
        redSoldier1.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Soldier, 1) * widthSquare - 0.45 * widthSquare
        // redSoldier2
        redSoldier2.height = widthSquare * 0.9
        redSoldier2.width = widthSquare * 0.9
        redSoldier2.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Soldier, 2) * widthSquare - 0.45 * widthSquare
        redSoldier2.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Soldier, 2) * widthSquare - 0.45 * widthSquare
        // redSoldier3
        redSoldier3.height = widthSquare * 0.9
        redSoldier3.width = widthSquare * 0.9
        redSoldier3.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Soldier, 3) * widthSquare - 0.45 * widthSquare
        redSoldier3.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Soldier, 3) * widthSquare - 0.45 * widthSquare
        // redSoldier4
        redSoldier4.height = widthSquare * 0.9
        redSoldier4.width = widthSquare * 0.9
        redSoldier4.x = shiftW + Qt_match.getXFigurePosition("red", Xiangqi.Soldier, 4) * widthSquare - 0.45 * widthSquare
        redSoldier4.y = shiftH + Qt_match.getYFigurePosition("red", Xiangqi.Soldier, 4) * widthSquare - 0.45 * widthSquare

        // white
        // whiteGeneral
        whiteGeneral.height = widthSquare * 0.9
        whiteGeneral.width = widthSquare * 0.9
        whiteGeneral.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.General, 0) * widthSquare - 0.45 * widthSquare
        whiteGeneral.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.General, 0) * widthSquare - 0.45 * widthSquare
        // whiteAdvisor0
        whiteAdvisor0.height = widthSquare * 0.9
        whiteAdvisor0.width = widthSquare * 0.9
        whiteAdvisor0.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Advisor, 0) * widthSquare - 0.45 * widthSquare
        whiteAdvisor0.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Advisor, 0) * widthSquare - 0.45 * widthSquare
        // whiteAdvisor1
        whiteAdvisor1.height = widthSquare * 0.9
        whiteAdvisor1.width = widthSquare * 0.9
        whiteAdvisor1.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Advisor, 1) * widthSquare - 0.45 * widthSquare
        whiteAdvisor1.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Advisor, 1) * widthSquare - 0.45 * widthSquare
        // whiteCannon0
        whiteCannon0.height = widthSquare * 0.9
        whiteCannon0.width = widthSquare * 0.9
        whiteCannon0.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Cannon, 0) * widthSquare - 0.45 * widthSquare
        whiteCannon0.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Cannon, 0) * widthSquare - 0.45 * widthSquare
        // whiteCannon1
        whiteCannon1.height = widthSquare * 0.9
        whiteCannon1.width = widthSquare * 0.9
        whiteCannon1.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Cannon, 1) * widthSquare - 0.45 * widthSquare
        whiteCannon1.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Cannon, 1) * widthSquare - 0.45 * widthSquare
        // whiteChariot0
        whiteChariot0.height = widthSquare * 0.9
        whiteChariot0.width = widthSquare * 0.9
        whiteChariot0.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Chariot, 0) * widthSquare - 0.45 * widthSquare
        whiteChariot0.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Chariot, 0) * widthSquare - 0.45 * widthSquare
        // whiteChariot1
        whiteChariot1.height = widthSquare * 0.9
        whiteChariot1.width = widthSquare * 0.9
        whiteChariot1.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Chariot, 1) * widthSquare - 0.45 * widthSquare
        whiteChariot1.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Chariot, 1) * widthSquare - 0.45 * widthSquare
        // whiteElephant0
        whiteElephant0.height = widthSquare * 0.9
        whiteElephant0.width = widthSquare * 0.9
        whiteElephant0.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Elephant, 0) * widthSquare - 0.45 * widthSquare
        whiteElephant0.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Elephant, 0) * widthSquare - 0.45 * widthSquare
        // whiteElephant1
        whiteElephant1.height = widthSquare * 0.9
        whiteElephant1.width = widthSquare * 0.9
        whiteElephant1.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Elephant, 1) * widthSquare - 0.45 * widthSquare
        whiteElephant1.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Elephant, 1) * widthSquare - 0.45 * widthSquare
        // whiteHorse0
        whiteHorse0.height = widthSquare * 0.9
        whiteHorse0.width = widthSquare * 0.9
        whiteHorse0.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Horse, 0) * widthSquare - 0.45 * widthSquare
        whiteHorse0.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Horse, 0) * widthSquare - 0.45 * widthSquare
        // whiteHorse1
        whiteHorse1.height = widthSquare * 0.9
        whiteHorse1.width = widthSquare * 0.9
        whiteHorse1.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Horse, 1) * widthSquare - 0.45 * widthSquare
        whiteHorse1.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Horse, 1) * widthSquare - 0.45 * widthSquare
        // whiteSoldier0
        whiteSoldier0.height = widthSquare * 0.9
        whiteSoldier0.width = widthSquare * 0.9
        whiteSoldier0.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Soldier, 0) * widthSquare - 0.45 * widthSquare
        whiteSoldier0.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Soldier, 0) * widthSquare - 0.45 * widthSquare
        // whiteSoldier1
        whiteSoldier1.height = widthSquare * 0.9
        whiteSoldier1.width = widthSquare * 0.9
        whiteSoldier1.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Soldier, 1) * widthSquare - 0.45 * widthSquare
        whiteSoldier1.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Soldier, 1) * widthSquare - 0.45 * widthSquare
        // whiteSoldier2
        whiteSoldier2.height = widthSquare * 0.9
        whiteSoldier2.width = widthSquare * 0.9
        whiteSoldier2.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Soldier, 2) * widthSquare - 0.45 * widthSquare
        whiteSoldier2.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Soldier, 2) * widthSquare - 0.45 * widthSquare
        // whiteSoldier3
        whiteSoldier3.height = widthSquare * 0.9
        whiteSoldier3.width = widthSquare * 0.9
        whiteSoldier3.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Soldier, 3) * widthSquare - 0.45 * widthSquare
        whiteSoldier3.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Soldier, 3) * widthSquare - 0.45 * widthSquare
        // whiteSoldier4
        whiteSoldier4.height = widthSquare * 0.9
        whiteSoldier4.width = widthSquare * 0.9
        whiteSoldier4.x = shiftW + Qt_match.getXFigurePosition("white", Xiangqi.Soldier, 4) * widthSquare - 0.45 * widthSquare
        whiteSoldier4.y = shiftH + Qt_match.getYFigurePosition("white", Xiangqi.Soldier, 4) * widthSquare - 0.45 * widthSquare
    }
}
