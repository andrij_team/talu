import QtQuick 2.0
import "qrc:/xiangqi/QML"

Rectangle {
    property int position;

    function redraw(){
        cross.height = widthSquare * 0.9
        cross.width = widthSquare * 0.9
        cross.x = shiftW + Math.floor(cross.position / 10) * widthSquare - 0.45 * widthSquare
        cross.y = shiftH + cross.position % 10 * widthSquare - 0.45 * widthSquare
    }

    id: cross
    radius: widthSquare
    width:  2 * radius
    height: 2 * radius
}
