#include "matchturnnotation.h"

MatchTurnNotation::MatchTurnNotation(QString redName, QString whiteName)
    : redPlayerName(redName), whitePlayerName(whiteName)
{}

void MatchTurnNotation::addTurn(int x1, int y1, int x2, int y2, Xiangqi::PlayerSide side)
{
    addTurn(std::make_pair(x1, y1), std::make_pair(x2, y2), side);
}

void MatchTurnNotation::addTurn(std::pair <int, int> startPosition,
                                std::pair <int, int> endPosition,
                                Xiangqi::PlayerSide side)
{
    TurnNotation newTurn;
    newTurn.startPosition = startPosition;
    newTurn.endPosition = endPosition;
    newTurn.side = side;
    turnList.push_back(newTurn);
}

void MatchTurnNotation::setMatchResult(Xiangqi::GameStatus status)
{
    gameStatus = status;
}

QDataStream& operator << (QDataStream &s, MatchTurnNotation &n)
{
    s << static_cast <int> (n.gameStatus) << n.redPlayerName
      << n.whitePlayerName << n.turnList.length();
    for (auto turn : n.turnList)
        s << turn;
    return s;
}

QDataStream& operator >> (QDataStream &s, MatchTurnNotation &n)
{
    int length;
    int gameStatus;
    TurnNotation turn;
    n.turnList.clear();
    s >> gameStatus >> n.redPlayerName >> n.whitePlayerName >> length;
    for (int i = 0; i < length; i++){
        s >> turn;
        n.turnList.push_back(turn);
    }
    n.gameStatus = static_cast <Xiangqi::GameStatus> (gameStatus);
    return s;
}
