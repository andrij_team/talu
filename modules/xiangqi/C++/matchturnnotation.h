#ifndef MATCHTURNNOTATION_H
#define MATCHTURNNOTATION_H

#include <QtCore>
#include "figure.h"

struct TurnNotation {
    std::pair <int, int> startPosition;
    std::pair <int, int> endPosition;
    Xiangqi::PlayerSide side;

    friend QDataStream& operator << (QDataStream &s, TurnNotation &n){
        return s << n.startPosition.first << n.startPosition.second
                 << n.endPosition.first << n.endPosition.second
                 << static_cast<int>(n.side);
    }

    friend QDataStream& operator >> (QDataStream &s, TurnNotation &n){
        int side, x1, x2, y1, y2;
        s >> x1 >> y1 >> x2 >> y2 >> side;
        n.startPosition = std::make_pair (x1, y1);
        n.endPosition = std::make_pair (x2, y2);
        n.side = static_cast <Xiangqi::PlayerSide> (side);
        return s;
    }
};

class MatchTurnNotation
{
private:
    QString   redPlayerName;
    QString whitePlayerName;
    QList <TurnNotation> turnList;
    Xiangqi::GameStatus gameStatus;

public:
    MatchTurnNotation(QString redName = "red", QString whiteName = "white");
    void addTurn(int x1, int y1, int x2, int y2, Xiangqi::PlayerSide side);
    void addTurn(std::pair <int, int> startPosition, std::pair <int, int> endPosition,
                 Xiangqi::PlayerSide side);
    void setMatchResult(Xiangqi::GameStatus status);
    Xiangqi::GameStatus getMatchResult() const { return gameStatus; }

    friend QDataStream& operator << (QDataStream &s, MatchTurnNotation &n);
    friend QDataStream& operator >> (QDataStream &s, MatchTurnNotation &n);
};

#endif // MATCHTURNNOTATION_H
