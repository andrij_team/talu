#include "player.h"

Player::Player(Xiangqi::PlayerControl control, QString playerName, Xiangqi::PlayerSide side)
    : playerName(playerName), side(side), control(control)
{
    int i(0);
    if (side == Xiangqi::PlayerSide::White)
        i = 9;

    figureList.clear();
    figureList.push_back(Figure(Xiangqi::FigureType::Chariot,  side, std::make_pair(0, i)));
    figureList.push_back(Figure(Xiangqi::FigureType::Horse,    side, std::make_pair(1, i)));
    figureList.push_back(Figure(Xiangqi::FigureType::Elephant, side, std::make_pair(2, i)));
    figureList.push_back(Figure(Xiangqi::FigureType::Advisor,  side, std::make_pair(3, i)));
    figureList.push_back(Figure(Xiangqi::FigureType::General,  side, std::make_pair(4, i)));
    figureList.push_back(Figure(Xiangqi::FigureType::Elephant, side, std::make_pair(6, i)));
    figureList.push_back(Figure(Xiangqi::FigureType::Advisor,  side, std::make_pair(5, i)));
    figureList.push_back(Figure(Xiangqi::FigureType::Horse,    side, std::make_pair(7, i)));
    figureList.push_back(Figure(Xiangqi::FigureType::Chariot,  side, std::make_pair(8, i)));

    figureList.push_back(Figure(Xiangqi::FigureType::Cannon,   side, std::make_pair(1, abs(i-2))));
    figureList.push_back(Figure(Xiangqi::FigureType::Cannon,   side, std::make_pair(7, abs(i-2))));

    figureList.push_back(Figure(Xiangqi::FigureType::Soldier,  side, std::make_pair(0, abs(i-3))));
    figureList.push_back(Figure(Xiangqi::FigureType::Soldier,  side, std::make_pair(2, abs(i-3))));
    figureList.push_back(Figure(Xiangqi::FigureType::Soldier,  side, std::make_pair(4, abs(i-3))));
    figureList.push_back(Figure(Xiangqi::FigureType::Soldier,  side, std::make_pair(6, abs(i-3))));
    figureList.push_back(Figure(Xiangqi::FigureType::Soldier,  side, std::make_pair(8, abs(i-3))));
}

void Player::recreatePlayer(Xiangqi::PlayerControl control, QString playerName)
{
    this->control = control;
    this->playerName = playerName;
}

std::pair <int, int> Player::getDefaultFigurePosition(Xiangqi::FigureType type, int number)
{
    int currentNumber = -1;
    for (auto figure : figureList){
        if (figure.getFigureType() == type){
            currentNumber++;
            if (currentNumber == number)
                return figure.getDefaultPosition();
        }
    }
    return std::make_pair(INT_MAX, INT_MAX);
}

void Player::setDefaultFigure()
{
    for (int i = 0; i < figureList.length(); ++i)
        figureList[i].setDefaultPosition();
}

Xiangqi::FigureType Player::getFigureTypeOnPlace(int x, int y)
{
    if (x < 0 || x > 8 || y < 0 || y > 9)
        return Xiangqi::FigureType::None;
    for (auto f : figureList)
        if (f.getXPosition() == x && f.getYPosition() == y)
            return f.getFigureType();
    return Xiangqi::FigureType::None;
}

Figure Player::getFigureOnPlace(int x, int y)
{
    for (auto f : figureList)
        if (std::make_pair(x, y) == f.getPosition())
            return f;
    return Figure();
}

Figure Player::getFigure(Xiangqi::FigureType type, int number)
{
    int currentNumber = -1;
    for (auto figure : figureList){
        if (figure.getFigureType() == type){
            currentNumber++;
            if (currentNumber == number)
                return figure;
        }
    }
    return Figure();
}

bool Player::setNewPositionToFugure(std::pair<int, int> oldCord, std::pair<int, int> newCord)
{
    for (int i = 0; i < figureList.length(); i++)
        if (figureList[i].getPosition() == oldCord){
            figureList[i].setPosition(newCord);
            return true;
        }
    return false;
}
