#ifndef PLAYER_H
#define PLAYER_H

#include <QtCore>
#include "figure.h"
#include <math.h>
#include "xiangqi.h"

class Player
{
private:
    QString playerName;
    QList <Figure> figureList;
    Xiangqi::PlayerSide side;
    Xiangqi::PlayerControl control;

public:
    Player(Xiangqi::PlayerControl control, QString playerName = "Guest",
           Xiangqi::PlayerSide side = Xiangqi::PlayerSide::None);

    std::pair <int, int> getDefaultFigurePosition(Xiangqi::FigureType type, int number);
    QList <Figure> getFigureList() const { return figureList; }
    Figure getFigure(Xiangqi::FigureType type, int number = 0);
    void setDefaultFigure();
    Xiangqi::PlayerControl getControl() const { return control; }
    Xiangqi::PlayerSide getSide() { return side; }
    Xiangqi::FigureType getFigureTypeOnPlace(int x, int y);
    Figure getFigureOnPlace(int x, int y);
    bool setNewPositionToFugure(std::pair <int, int> oldCord, std::pair <int, int> newCord);
    void recreatePlayer(Xiangqi::PlayerControl control, QString playerName = "Guest");
};

#endif // PLAYER_H
