#include "xiangqimodule.h"

XiangqiModule::XiangqiModule(){}

void XiangqiModule::registrQmlProperty()
{
    emit setContextProperty("Qt_Xiangqi", new Xiangqi);
    qmlRegisterType<Xiangqi>("Xiangqi", 1, 0, "Xiangqi");
}

bool XiangqiModule::isCoopPlayerPresent()
{
    return true;
}

QString XiangqiModule::getName()
{
    return "Xiangqi";
}

QQuickItem* XiangqiModule::createGameItem(QQmlEngine *engine)
{
    QQmlComponent component(engine, QUrl("qrc:/xiangqi/QML/Desk.qml"));
    qDebug() << component.errors() << component.isReady();
    match = new MatchManager;
    emit setContextProperty("Qt_match", match);
    item = qobject_cast <QQuickItem*> (component.create());
    match->setDesk(static_cast <QObject*> (item));
    return item;
}
