#ifndef FIGURE_H
#define FIGURE_H

#include <QtCore>
#include <QtQml>
#include "xiangqi.h"

class Figure : public QObject
{
    Q_OBJECT
private:
    Xiangqi::FigureType figureType;
    Xiangqi::PlayerSide side;
    std::pair <int, int> position;
    std::pair <int, int> defaultPosition;

public:
    Figure(Xiangqi::FigureType figureType, Xiangqi::PlayerSide side,
           std::pair <int, int> position = std::make_pair(INT_MAX, INT_MAX));
    Figure(std::pair <int, int> position = std::make_pair(INT_MAX, INT_MAX));
    Figure(const Figure &f);

    std::pair <int, int> getPosition() const;
    std::pair <int, int> getDefaultPosition() const;
    bool isOnDesk() const;
    Xiangqi::PlayerSide getSide() const;
    Xiangqi::FigureType getFigureType() const;

    void setPosition(std::pair <int, int> position);
    void setDefaultPosition();

    Q_INVOKABLE int getXPosition() { return position.first;  }
    Q_INVOKABLE int getYPosition() { return position.second; }
};

#endif // FIGURE_H
