#include "figure.h"

Figure::Figure(Xiangqi::FigureType figureType, Xiangqi::PlayerSide side,
               std::pair<int, int> position)
    : figureType(figureType), side(side),
      position(position), defaultPosition(position)
{}

Figure::Figure(std::pair<int, int> position)
    : figureType(Xiangqi::FigureType::None),
      position(position), defaultPosition(position)
{}

Figure::Figure(const Figure &f)
{
    figureType = f.figureType;
    side = f.side;
    position = f.position;
    defaultPosition = f.defaultPosition;
}

std::pair<int, int> Figure::getPosition() const
{
    return position;
}

std::pair<int, int> Figure::getDefaultPosition() const
{
    return defaultPosition;
}

bool Figure::isOnDesk() const
{
    return position.first >= 0 && position.first <= 8
            && position.second >= 0 && position.second <= 9;
}

Xiangqi::PlayerSide Figure::getSide() const
{
    return side;
}

void Figure::setDefaultPosition()
{
    setPosition(defaultPosition);
}

Xiangqi::FigureType Figure::getFigureType() const
{
    return figureType;
}

void Figure::setPosition(std::pair<int, int> position)
{
    this->position = position;
}
