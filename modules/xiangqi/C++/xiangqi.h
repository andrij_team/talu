#ifndef XIANGQI_H
#define XIANGQI_H

#include <QtCore>

class Xiangqi : public QObject
{
    Q_OBJECT
public:
    enum class FigureType {
        General  = 0,
        Advisor  = 1,
        Cannon   = 2,
        Chariot  = 3,
        Elephant = 4,
        Horse    = 5,
        Soldier  = 6,
        None     = 7,
    };
    Q_ENUMS(FigureType)

    enum class GameStatus {
        InProcess = 0,
        RedWin = 1,
        WhiteWin = 2,
        Draw = 3,
    };
    Q_ENUMS(GameStatus)

    enum class PlayerSide {
        Red = 0,
        White = 1,
        None = 2,
        OutOfDesk = 3,
    };
    Q_ENUMS(PlayerSide)

    enum class PlayerControl {
        AI = 0,
        localPlayer = 1,
        networkPlayer = 2,
    };
    Q_ENUMS(PlayerControl)

    enum class Version {
        V_0_1 = 0,
    };
    Q_ENUMS(Version)

    Q_INVOKABLE static QString getImagePath(QString color, int type)
    {
        QString imagePath = "qrc:/xiangqi/svg/";
        switch (static_cast<FigureType>(type)) {
        case FigureType::Advisor:  imagePath += "advisor";  break;
        case FigureType::Cannon:   imagePath += "cannon";   break;
        case FigureType::Chariot:  imagePath += "chariot";  break;
        case FigureType::Elephant: imagePath += "elephant"; break;
        case FigureType::General:  imagePath += "general";  break;
        case FigureType::Horse:    imagePath += "horse";    break;
        case FigureType::Soldier:  imagePath += "soldier";  break;
        default:;
        }
        color == "red" ? imagePath += "R" : imagePath += "W";
        imagePath += ".svg.png";
        return imagePath;
    }
};

#endif // XIANGQI_H
