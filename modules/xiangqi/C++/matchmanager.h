#ifndef MATCHMANAGER_H
#define MATCHMANAGER_H

#include <QtCore>
#include <QtGui>
#include <QQuickView>
#include <QQmlContext>
#include <QQuickItem>

#include "matchturnnotation.h"
#include "player.h"
#include "figure.h"

class MatchManager : public QObject
{
    Q_OBJECT
private:
    QObject *desk;
    Player *topPlayer;
    Player *bottomPlayer;
    Player *currentPlayer;

    std::pair <int, int> cordByLastChooseFigure;
    QList <std::pair <int, int>> possibleTurnPlaceList;

    void repaint(Xiangqi::PlayerSide side, QString text);

    //void victory(Figure figure);

    void checkVictory();
    void switchTurnSide();
    void setDefaultField();

    Player* getNotCurrentPlayer();

    Xiangqi::FigureType getFigureTypeByCoordinate(int x, int y);
    Xiangqi::PlayerSide getFigureSideByCoordinate(int x, int y);
    Figure getFigureByCoordinate(int x, int y);
    bool replaceFigure(int x, int y);
    void findPossibleTurn(int x, int y);

public:
    MatchManager();
    void setDesk(QObject* desk);

    Q_INVOKABLE int getXFigurePosition(QString color, int type, int number);
    Q_INVOKABLE int getYFigurePosition(QString color, int type, int number);
    Q_INVOKABLE void clickToDesk(int x, int y);

    Q_INVOKABLE void startOnePlayerMatch();
    Q_INVOKABLE void startTwoPlayerMatch(QString redName, QString whiteName);
    Q_INVOKABLE void startLocalMatch();

signals:
    void go();
};

#endif // MATCHMANAGER_H
