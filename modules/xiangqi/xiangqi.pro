QT += qml
QT += quick

CONFIG += c++14
CONFIG += plugin
CONFIG += qtquickcompiler
DESTDIR = $$PWD/../../../TaLu_Bin/modules
TARGET = xiangqi
TEMPLATE = lib

OTHER_FILES += $$PWD/QML/*.qml

HEADERS += \
    C++/logic.h \
    C++/figure.h \
    C++/player.h \
    C++/xiangqi.h \
    C++/matchmanager.h \
    C++/xiangqimodule.h \
    C++/matchturnnotation.h \

SOURCES += \
    C++/logic.cpp \
    C++/figure.cpp \
    C++/player.cpp \
    C++/matchmanager.cpp \
    C++/xiangqimodule.cpp \
    C++/matchturnnotation.cpp \

RESOURCES += \
    xiangqi.qrc
