QT += qml
QT += quick

CONFIG += c++14
CONFIG += plugin
CONFIG += qtquickcompiler
DESTDIR = $$PWD/../../../TaLu_Bin/modules
TARGET = checkers
TEMPLATE = lib

OTHER_FILES += $$PWD/QML/*.qml

HEADERS += \
    C++/checkersmodule.h \
    C++/checkerslogic.h \
    C++/player.h \
    C++/checker.h

SOURCES += \
    C++/checkersmodule.cpp \
    C++/checkerslogic.cpp \
    C++/player.cpp \
    C++/checker.cpp

RESOURCES += \
    checkers_resource.qrc
