#include "checkerslogic.h"

CheckersLogic::CheckersLogic(QQuickItem* desk, QObject *parent)
    : QObject(parent), desk(desk)
{
    white = new Player(1);
    black = new Player(-1);
}

void CheckersLogic::clickToDesk(int x, int y)
{
    std::pair <int, int> currentClick = std::make_pair(x, y);
    qDebug() << "currentLick" << currentClick << possibleTurnList;
    for (auto possibleTurn : possibleTurnList)
        if (possibleTurn == currentClick){
            // move figure
            qDebug() << "replace checker " << currentPlayerPtr()->replaceChecker(lastChooseChecker, currentClick);
            if (std::abs(lastChooseChecker.first - currentClick.first) != 1)
                for (auto c : possibleTurnList)
                    if (notCurrentPlayerPtr()->isCheckerPresentAtCoor(c))
                        if (c.first > lastChooseChecker.first && c.first < currentClick.first
                                || c.first < lastChooseChecker.first && c.first > currentClick.first)
                            notCurrentPlayerPtr()->removeChecker(c);

            // victory player show
            if (checkVictory()){}
            // end of turn
            else
                newTurn();
            // clear possible desk
            QMetaObject::invokeMethod(desk, "clearDesk");
            possibleTurnList.clear();
            return;
        }

    // choose new figure
    possibleTurnList.clear();
    // get directions
    auto checker = currentPlayer->getCheckerAtCoor(currentClick);
    if (checker.first){
        QList <std::pair <int, int>> list;
        if (checker.second->isQuenn())
            list = findPossibleQueenTurn(currentClick);
        else
            list = findPossibleSoldierTurn(currentClick);

        // send turn visible to QML
        QList <int> playerList;
        QList <int> enemyList;
        QList <int> pathList;
        for (std::pair <int, int> c : list){
            auto p = getPlayerCheckerOnCoor(c);
            if (p == nullptr)
                pathList.push_back(c.first * 10 + c.second);
            else if (p == currentPlayer)
                playerList.push_back(c.first * 10 + c.second);
            else
                enemyList.push_back(c.first * 10 + c.second);
        }
        possibleTurnList = list;
        lastChooseChecker = checker.second->getPosition();
        QMetaObject::invokeMethod(desk, "setPossibleTurn",
                                  Q_ARG(QVariant, QVariant::fromValue(playerList)),
                                  Q_ARG(QVariant, QVariant::fromValue(pathList)),
                                  Q_ARG(QVariant, QVariant::fromValue(enemyList)));
    }
    // clear desk
    else
        QMetaObject::invokeMethod(desk, "clearDesk");
}

QList <std::pair <int, int>> CheckersLogic::findPossibleQueenTurn(std::pair <int, int> coor)
{
    QList <std::pair <int, int>> list;
    return list;
}

QList <std::pair <int, int>> CheckersLogic::findPossibleSoldierTurn(std::pair<int, int> coor)
{
    QList <std::pair <int, int>> list;
    // left turn
    if (coor.first > 0){
        // test free move to 1 point
        auto side = getPlayerCheckerOnCoor(coor.first - 1,
                                           coor.second + currentPlayer->getMoveKoof());
        if (side == nullptr)
            list.push_back(std::make_pair(coor.first - 1,
                                          coor.second + currentPlayer->getMoveKoof()));
        else if (side == notCurrentPlayerPtr())
            if (coor.first - 2 >= 0)
                if (getPlayerCheckerOnCoor(coor.first - 2,
                                           coor.second + 2 * currentPlayer->getMoveKoof()) == nullptr){
                    list.push_back(std::make_pair(coor.first - 1,
                                                  coor.second + currentPlayer->getMoveKoof()));
                    list.push_back(std::make_pair(coor.first - 2,
                                                  coor.second + 2 * currentPlayer->getMoveKoof()));
                    //list += findPossibleSoldierTurn(std::make_pair(coor.first - 2,
                      //                                             coor.second + 2 * currentPlayer->getMoveKoof()));
                }
        side = getPlayerCheckerOnCoor(coor.first - 1,
                                      coor.second - currentPlayer->getMoveKoof());
        if (side == notCurrentPlayerPtr())
            if (coor.first - 2 >= 0)
                if (getPlayerCheckerOnCoor(coor.first - 2,
                                           coor.second - 2 * currentPlayer->getMoveKoof()) == nullptr){
                    list.push_back(std::make_pair(coor.first - 1,
                                                  coor.second + currentPlayer->getMoveKoof()));
                    list.push_back(std::make_pair(coor.first - 2,
                                                  coor.second - 2 * currentPlayer->getMoveKoof()));
                    //list += findPossibleSoldierTurn(std::make_pair(coor.first - 2,
                      //                                             coor.second - 2 * currentPlayer->getMoveKoof()));
                }
    }

    // right turn
    if (coor.first < 10){
        // test free move to 1 point
        auto side = getPlayerCheckerOnCoor(coor.first + 1,
                                           coor.second + currentPlayer->getMoveKoof());
        if (side == nullptr)
            list.push_back(std::make_pair(coor.first + 1,
                                          coor.second + currentPlayer->getMoveKoof()));
        else if (side == notCurrentPlayerPtr())
            if (coor.first + 2 < 10)
                if (getPlayerCheckerOnCoor(coor.first + 2,
                                           coor.second + 2 * currentPlayer->getMoveKoof()) == nullptr){
                    list.push_back(std::make_pair(coor.first + 1,
                                                  coor.second + currentPlayer->getMoveKoof()));
                    list.push_back(std::make_pair(coor.first + 2,
                                                  coor.second + 2 * currentPlayer->getMoveKoof()));
                    //list += findPossibleSoldierTurn(std::make_pair(coor.first + 2,
                      //                                             coor.second + 2 * currentPlayer->getMoveKoof()));
                }
        side = getPlayerCheckerOnCoor(coor.first + 1,
                                      coor.second - currentPlayer->getMoveKoof());
        if (side == notCurrentPlayerPtr())
            if (coor.first + 2 < 10)
                if (getPlayerCheckerOnCoor(coor.first + 2,
                                           coor.second - 2 * currentPlayer->getMoveKoof()) == nullptr){
                    list.push_back(std::make_pair(coor.first + 1,
                                                  coor.second - currentPlayer->getMoveKoof()));
                    list.push_back(std::make_pair(coor.first + 2,
                                                  coor.second - 2 * currentPlayer->getMoveKoof()));
                    //list += findPossibleSoldierTurn(std::make_pair(coor.first + 2,
                      //                                             coor.second - 2 * currentPlayer->getMoveKoof()));
                }
    }
    return list;
}

Player* CheckersLogic::currentPlayerPtr()
{
    if (white == currentPlayer)
        return white;
    return black;
}

Player* CheckersLogic::notCurrentPlayerPtr()
{
    if (white == currentPlayer)
        return black;
    return white;
}

void CheckersLogic::newTurn()
{
    currentPlayer = notCurrentPlayerPtr();
}

bool CheckersLogic::checkVictory()
{
    return false;
}

void CheckersLogic::gameStart()
{
    black->removeAllChecker();
    white->removeAllChecker();
    currentPlayer = white;
    possibleTurnList.clear();
    for (int i = 0; i < 15; i++){
        // black checker
        QMetaObject::invokeMethod(desk, "createChecker", Qt::AutoConnection,
                                  Q_ARG(QVariant, "white"),
                                  Q_ARG(QVariant, 2 * (i % 5) + static_cast <int> (i / 5) % 2),
                                  Q_ARG(QVariant, static_cast <int> (i / 5)));
        // white checker
        QMetaObject::invokeMethod(desk, "createChecker",
                                  Q_ARG(QVariant, "black"),
                                  Q_ARG(QVariant, 2 * ((i + 35) % 5) + static_cast <int> ((i + 35) / 5) % 2),
                                  Q_ARG(QVariant, static_cast <int> ((i + 35) / 5)));
    }
}

void CheckersLogic::setNewFigure(QString side, QQuickItem *item)
{
    if (side == "black")
        black->setFigure(item);
    else
        white->setFigure(item);
}

Player* CheckersLogic::getPlayerCheckerOnCoor(int x, int y)
{
    return getPlayerCheckerOnCoor(std::make_pair(x, y));
}

Player* CheckersLogic::getPlayerCheckerOnCoor(std::pair<int, int> coor)
{
    if (white->isCheckerPresentAtCoor(coor))
        return white;
    if (black->isCheckerPresentAtCoor(coor))
        return black;
    return nullptr;
}
