#include "checker.h"

Checker::Checker(QQuickItem *item)
    : item(item), quenn(false)
{
    setPosition(item->property("pos_x").toInt(), item->property("pos_y").toInt());
}

Checker::~Checker()
{
    delete item;
}

bool Checker::isCoincides(std::pair <int, int> pos)
{
    return pos == this->pos;
}

bool Checker::isCoincides(int x, int y)
{
    return isCoincides(std::make_pair(x, y));
}

bool Checker::isQuenn()
{
    return quenn;
}

void Checker::setQuenn()
{
    quenn = true;
    QMetaObject::invokeMethod(item, "doQueen");
}

void Checker::setPosition(int x, int y)
{
    setPosition(std::make_pair(x, y));
}

void Checker::setPosition(std::pair <int, int> pos)
{
    this->pos = pos;
    item->setProperty("pos_x", QVariant(pos.first));
    item->setProperty("pos_y", QVariant(pos.second));
    QMetaObject::invokeMethod(item, "redraw");
}

std::pair <int, int> Checker::getPosition()
{
    return pos;
}
