#ifndef CHECKER_H
#define CHECKER_H

#include <QQuickItem>

class Checker
{
private:
    QQuickItem *item;
    std::pair <int, int> pos;
    bool quenn;

public:
    Checker(QQuickItem *item);
    ~Checker();

    bool isCoincides(std::pair <int, int> pos);
    bool isCoincides(int x, int y);
    bool isQuenn();
    void setQuenn();
    void setPosition(int x, int y);
    void setPosition(std::pair <int, int> pos);
    std::pair <int, int> getPosition();
};

#endif // CHECKER_H
