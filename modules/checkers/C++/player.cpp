#include "player.h"

Player::Player(int moveKoof) : moveKoof(moveKoof)
{
    //
}

int Player::getFigureCountOnDesk()
{
    return figureList.length();
}

void Player::removeAllChecker()
{
    for (auto c : figureList)
        delete c;
    figureList.clear();
}

void Player::setFigure(QQuickItem *item)
{
    figureList.push_back(new Checker(item));
}

bool Player::removeChecker(int x, int y)
{
    return removeChecker(std::make_pair(x, y));
}

bool Player::replaceChecker(int x1, int y1, int x2, int y2)
{
    return replaceChecker(std::make_pair(x1, y1), std::make_pair(x2, y2));
}

bool Player::isCheckerPresentAtCoor(int x, int y)
{
    return isCheckerPresentAtCoor(std::make_pair(x, y));
}

bool Player::removeChecker(std::pair<int, int> coor)
{
    for (auto c : figureList)
        if (c->isCoincides(coor)){
                delete c;
                figureList.removeOne(c);
                return true;
            }
    return false;
}

bool Player::replaceChecker(std::pair<int, int> coor1, std::pair<int, int> coor2)
{
    for (auto c : figureList)
        if (c->isCoincides(coor1)){
            c->setPosition(coor2);
            return true;
        }
    return false;
}

bool Player::isCheckerPresentAtCoor(std::pair<int, int> coor)
{
    for (auto c : figureList)
        if (c->isCoincides(coor))
            return true;
    return false;
}

std::pair <bool, Checker*> Player::getCheckerAtCoor(int x, int y)
{
    return Player::getCheckerAtCoor(std::make_pair(x, y));
}

std::pair <bool, Checker*> Player::getCheckerAtCoor(std::pair<int, int> coor)
{
    for (auto c : figureList)
        if (c->isCoincides(coor))
            return std::make_pair(true, c);
    return std::make_pair(false, nullptr);
}
