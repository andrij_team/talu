#ifndef PLAYER_H
#define PLAYER_H

#include "checker.h"
#include <QtCore>
#include <QtQuick>

class Player
{
private:
    QString side;
    QString name;
    int score;
    int moveKoof;

    QList <Checker*> figureList;

public:
    Player(int moveKoof);

    void setFigure(QQuickItem* item);
    int getFigureCountOnDesk();
    int getMoveKoof() { return moveKoof; }
    void removeAllChecker();
    bool removeChecker(int x, int y);
    bool removeChecker(std::pair <int, int> coor);
    bool replaceChecker(int x1, int y1, int x2, int y2);
    bool replaceChecker(std::pair <int, int> coor1, std::pair <int, int> coor2);
    bool isCheckerPresentAtCoor(int x, int y);
    bool isCheckerPresentAtCoor(std::pair <int, int> coor);
    std::pair<bool, Checker*> getCheckerAtCoor(int x, int y);
    std::pair<bool, Checker*> getCheckerAtCoor(std::pair <int, int> coor);
};

#endif // PLAYER_H
