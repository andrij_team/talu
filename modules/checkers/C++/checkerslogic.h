#ifndef CHECKERSLOGIC_H
#define CHECKERSLOGIC_H

#include <QtCore>
#include <QtQuick>

#include "player.h"

class CheckersLogic : public QObject
{
    Q_OBJECT
private:
    QQuickItem* desk;
    Player *black;
    Player *white;
    Player *currentPlayer;

    QList <std::pair <int, int>> possibleTurnList;
    std::pair <int, int> lastChooseChecker;

    Player* currentPlayerPtr();
    Player* notCurrentPlayerPtr();
    void newTurn();
    bool checkVictory();
    Player* getPlayerCheckerOnCoor(int x, int y);
    Player* getPlayerCheckerOnCoor(std::pair <int, int> coor);
    QList <std::pair <int, int>> findPossibleSoldierTurn(std::pair <int, int> coor);
    QList <std::pair <int, int>> findPossibleQueenTurn(std::pair <int, int> coor);

public:
    CheckersLogic(QQuickItem* desk, QObject *parent = 0);
    void gameStart();

    Q_INVOKABLE void clickToDesk(int x, int y);
    Q_INVOKABLE void setNewFigure(QString side, QQuickItem* item);
};

#endif // CHECKERSLOGIC_H
