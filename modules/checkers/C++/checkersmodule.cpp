#include "checkersmodule.h"

CheckersModule::CheckersModule(){}

void CheckersModule::registrQmlProperty()
{
    //
}

void CheckersModule::closeModule(){}

QString CheckersModule::getName()
{
    return "Checkers";
}

QQuickItem* CheckersModule::createGameItem(QQmlEngine *engine)
{
    QQmlComponent component(engine, QUrl("qrc:/checkers/QML/main.qml"));
    qDebug() << component.errors() << component.isReady();
    item = qobject_cast <QQuickItem*> (component.create());
    logic = new CheckersLogic(item->findChild <QQuickItem*> ("desk"), this);
    emit setContextProperty("Checkers", logic);
    logic->gameStart();
    return item;
}
