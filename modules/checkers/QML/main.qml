import QtQuick 2.0
import "qrc:/checkers/QML"

Rectangle {
    id: mainCheckers
    anchors.fill: parent
    color: "#505050"

    property double figureSize: Math.min(height, width) / 10

    Desk {
        id: desk
        anchors.fill: parent
        objectName: "desk"
    }

    function redraw(){
        figureSize = Math.min(height, width) / 10
        desk.redraw();
    }

    onHeightChanged: redraw();
    onWidthChanged:  redraw();
}
