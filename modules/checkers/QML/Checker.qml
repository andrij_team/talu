import QtQuick 2.0

Item {
    id: checker

    property int pos_x;
    property int pos_y;
    property string side;

    function doSoldier(){
        quenn.visible = false;
    }

    function doQuenn(){
        quenn.visible = true;
    }

    function isQuenn(){
        return quenn.visible;
    }

    function getObject(){
        Checkers.setNewFigure(side, checker);
    }

    function redraw(){
        checker.height = figureSize
        checker.width  = figureSize
        checker.x = pos_x * figureSize
        checker.y = pos_y * figureSize
    }

    Rectangle {
        width:  0.9 * parent.width
        height: 0.9 * parent.height
        radius: width / 2
        anchors.centerIn: parent
        color: side

        Rectangle {
            id: quenn
            visible: false
            radius: figureSize / 2
            width:  2 * radius
            height: 2 * radius
            anchors.centerIn: parent
            border.width: 2
            border.color: "navy"
        }
    }
}
