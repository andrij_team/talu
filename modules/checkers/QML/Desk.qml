import QtQuick 2.5

Rectangle {
    function getColorByIndex(index){
        if (((index % 10) + Math.floor(index / 10)) % 2 == 0)
            return "darkgrey";
        else
            return "khaki";
    }

    function redraw(){
        var listChild = checkersFigureDesk.children;
        for (var i = 0; i < listChild.length; i++)
            listChild[i].redraw();
    }

    function createChecker(/*string*/side, /*int*/pos_x, /*int*/pos_y){
        var s = Qt.createComponent("Checker.qml").createObject(checkersFigureDesk, {
                                                                   "side"  : side,
                                                                   "pos_x" : pos_x,
                                                                   "pos_y" : pos_y });
        s.getObject();
        s.redraw();
    }

    function clearDesk(){
        var listChild = possibleTurnRect.children;
        for (var i = 0; i < listChild.length; i++)
            listChild[i].destroy();
        redraw();
    }

    function setPossibleTurn(player, path, enemy){
        // clear last possible component
        clearDesk();

        for (var i = 0; i < player.length; i++)
            Qt.createComponent("PossibleTurnRect.qml").createObject(possibleTurnRect,
                                                                    { "pos_x" : Math.floor(player[i] / 10),
                                                                      "pos_y" : player[i] % 10,
                                                                      "color" : "darkgreen" });
        // set possible path to free rect
        for (i = 0; i < path.length; i++)
            Qt.createComponent("PossibleTurnRect.qml").createObject(possibleTurnRect,
                                                                    { "pos_x" : Math.floor(path[i] / 10),
                                                                      "pos_y" : path[i] % 10,
                                                                      "color" : "green" });
        // set possible path to free rect
        for (i = 0; i < enemy.length; i++)
            Qt.createComponent("PossibleTurnRect.qml").createObject(possibleTurnRect,
                                                                    { "pos_x" : Math.floor(enemy[i] / 10),
                                                                      "pos_y" : enemy[i] % 10,
                                                                      "color" : "red" });
        redraw();
    }

    color: "#505050"

    Rectangle {
        id: checkersDeskIntoRect
        z: 0
        anchors.centerIn: parent
        height: Math.min(parent.height, parent.width)
        width:  Math.min(parent.height, parent.width)

        Grid {
            id: grid
            columns: 10

            Repeater {
                id: deskRepeater
                model: 100
                Rectangle {
                    width: checkersDeskIntoRect.width / 10
                    height: checkersDeskIntoRect.height / 10
                    color: getColorByIndex(index)
                }
            }
        }
    }

    MouseArea {
        height: checkersDeskIntoRect.height
        width: checkersDeskIntoRect.width
        anchors.centerIn: parent
        z: 65000
        onClicked: {
            for (var i = 0; i < 100; i++){
                var item = deskRepeater.itemAt(i);
                if (mouseX > item.x && mouseY > item.y
                        && mouseX < item.x + item.width
                        && mouseY < item.y + item.height){
                    Checkers.clickToDesk(i % 10, Math.floor(i / 10));
                    break;
                }
            }
        }
    }

    Item {
        id: possibleTurnRect
        z: 1
        height: checkersDeskIntoRect.height
        width:  checkersDeskIntoRect.width
        x: checkersDeskIntoRect.x
        y: checkersDeskIntoRect.y
    }

    Item {
        id: checkersFigureDesk
        z: 2
        height: checkersDeskIntoRect.height
        width:  checkersDeskIntoRect.width
        x: checkersDeskIntoRect.x
        y: checkersDeskIntoRect.y
    }
}
