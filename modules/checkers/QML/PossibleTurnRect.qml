import QtQuick 2.0

Rectangle {
    property int pos_x: 0
    property int pos_y: 0
    height: parent.height / 10
    width: parent.width / 10
    x: parent.width * pos_x / 10
    y: parent.height * pos_y / 10
    opacity: 0.4
}
