QT += qml
QT += quick

CONFIG += c++14
CONFIG += plugin
CONFIG += qtquickcompiler
DESTDIR = $$PWD/../../../TaLu_Bin/modules
TARGET = nought_and_crosses
TEMPLATE = lib

OTHER_FILES += $$PWD/QML/*.qml

SOURCES += \
    C++/noughtandcrossesmodule.cpp \
    C++/gamelogic.cpp

HEADERS += \
    C++/noughtandcrossesmodule.h \
    C++/playerinfo.h \
    C++/gamelogic.h

RESOURCES += \
    n_and_c_resource.qrc
