import QtQuick 2.0

Rectangle {
    anchors.centerIn: parent
    width: parent.width*4 / 7
    height: parent.height*4 / 7

    Canvas {
        anchors.fill: parent

        // draw cross
        onPaint: {
            var context = getContext("2d");
            context.beginPath();
            context.lineWidth = 3;
            context.strokeStyle = lineColor;

            context.moveTo(0, 0);
            context.lineTo(parent.width, parent.height);
            context.stroke();

            context.moveTo(parent.width, 0);
            context.lineTo(0, parent.height);
            context.stroke();
        }
    }
}
