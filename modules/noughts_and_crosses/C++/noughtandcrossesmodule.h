#ifndef NOUGHTANDCROSSESMODULE_H
#define NOUGHTANDCROSSESMODULE_H

#include <QtCore/QObject>
#include <QtCore/QPluginLoader>
#include <QtCore/QString>
#include <QtCore/QtPlugin>
#include <QtCore/QUrl>

#include <QtQml/QQmlEngine>
#include <QtQuick/QQuickItem>

class BasePlugin : public QObject
{
    Q_OBJECT
public:
    virtual ~BasePlugin(){}
    virtual void registrQmlProperty() = 0;
    virtual QString getName() = 0;

    virtual bool isAboutPresent()      { return false; }
    virtual bool isLocalPresent()      { return false; }
    virtual bool isNetworkPresent()    { return false; }
    virtual bool isSaveModePresent()   { return false; }
    virtual bool isSettingsPresent()   { return false; }
    virtual bool isOnePlayerPresent()  { return false; }
    virtual bool isCoopPlayerPresent() { return false; }

    virtual void closeModule() = 0;
    virtual QQuickItem* createGameItem(QQmlEngine *engine) = 0;
    virtual QQuickItem* createAboutItem(QQmlEngine *engine)    { Q_UNUSED(engine); return nullptr; }
    virtual QQuickItem* createSettingsItem(QQmlEngine *engine) { Q_UNUSED(engine); return nullptr; }

signals:
    std::pair <bool, QString>   saveData(QString title, QByteArray data);
    std::pair <bool, QByteArray> getData(QString title);
    void setContextProperty(QString name, QObject *obj);
    QSize getMenuSize();
};

#define BasePlugin_iid "org.talu.baseplugin"
Q_DECLARE_INTERFACE(BasePlugin, BasePlugin_iid)

#include "playerinfo.h"
#include "gamelogic.h"

class NoughtAndCrossesModule : public BasePlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.talu.baseplugin")
    Q_INTERFACES(BasePlugin)
private:
    PlayerInfo *player1;
    PlayerInfo *player2;
    PlayerInfo *currentPlayer;

    GameLogic *gameLogic;
    QQuickItem *item;

    void draw();
    void victory(Figure figure);

public:
    NoughtAndCrossesModule();
    void closeModule() Q_DECL_OVERRIDE;
    void registrQmlProperty() Q_DECL_OVERRIDE;
    bool isLocalPresent() Q_DECL_OVERRIDE;
    bool isNetworkPresent() Q_DECL_OVERRIDE;
    bool isOnePlayerPresent() Q_DECL_OVERRIDE;
    bool isCoopPlayerPresent() Q_DECL_OVERRIDE;
    QQuickItem* createGameItem(QQmlEngine *engine) Q_DECL_OVERRIDE;
    QString getName() Q_DECL_OVERRIDE;

    Q_INVOKABLE void chooseSquare(int index, QObject *obj);
    Q_INVOKABLE void resetProgress();
    Q_INVOKABLE int getMenuHeight();
};

#endif // NOUGHTANDCROSSESMODULE_H
