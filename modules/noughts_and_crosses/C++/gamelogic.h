#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include <QtCore/QObject>

#include "playerinfo.h"

class GameLogic : public QObject
{
    Q_OBJECT
private:
    Figure figureMas[3][3];

    bool isSquareFree(int index);

public:
    explicit GameLogic(QObject *parent = 0);

    void clear();
    bool checkForVictory();
    bool setFigure(Figure figure, int index);

signals:
    void draw();
    void victory(Figure);
};

#endif // GAMELOGIC_H
