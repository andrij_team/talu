#include "noughtandcrossesmodule.h"

NoughtAndCrossesModule::NoughtAndCrossesModule()
    : gameLogic(nullptr), item(nullptr),
      player1(nullptr), player2(nullptr)
{}

void NoughtAndCrossesModule::closeModule()
{
    if (gameLogic != nullptr){
        delete gameLogic;
        gameLogic = nullptr;
    }
    if (player1 != nullptr){
        delete player1;
        player1 = nullptr;
    }
    if (player2 != nullptr){
        delete player2;
        player2 = nullptr;
    }
    if (item != nullptr){
        delete item;
        item = nullptr;
    }
}

void NoughtAndCrossesModule::registrQmlProperty()
{
    qmlRegisterType<PlayerInfo>("PlayerInfo", 1, 0, "PlayerInfo");
    emit setContextProperty("N_and_C", this);
}

bool NoughtAndCrossesModule::isLocalPresent()
{
    return false;
}

bool NoughtAndCrossesModule::isNetworkPresent()
{
    return false;
}

bool NoughtAndCrossesModule::isOnePlayerPresent()
{
    return false;
}

bool NoughtAndCrossesModule::isCoopPlayerPresent()
{
    return true;
}

QQuickItem* NoughtAndCrossesModule::createGameItem(QQmlEngine *engine)
{
    gameLogic = new GameLogic(this);
    player1 = new PlayerInfo("Player 1", Figure::Cross);
    player2 = new PlayerInfo("Player 2", Figure::Nought);
    currentPlayer = player1;

    QQmlComponent component(engine, QUrl("qrc:/n_and_c/QML/main.qml"));
    qDebug() << component.errors() << component.isReady();

    emit setContextProperty("player1", player1);
    emit setContextProperty("player2", player2);

    QObject::connect(gameLogic, &GameLogic::victory,  this, &NoughtAndCrossesModule::victory);
    QObject::connect(gameLogic, &GameLogic::draw, this, &NoughtAndCrossesModule::draw);

    item = qobject_cast <QQuickItem*> (component.create());
    return item;
}

QString NoughtAndCrossesModule::getName()
{
    return "Nought and Crosses";
}

int NoughtAndCrossesModule::getMenuHeight()
{
    QSize size = getMenuSize();
    return getMenuSize().height();
}

void NoughtAndCrossesModule::draw()
{
    // show message anout dead heat, after 3 seconds hide it
    QMetaObject::invokeMethod(item, "showMessage", Q_ARG(QVariant, "Draw"));
    QTimer::singleShot(3000, this, [=](){
        QMetaObject::invokeMethod(item, "hideMessage");
        gameLogic->clear();
        QMetaObject::invokeMethod(item, "clearDesk");
    });
}

void NoughtAndCrossesModule::victory(Figure figure)
{
    PlayerInfo* winPlayer;
    // update score and set first turn to loser
    if (player1->getFigure() == figure){
        player1->setScore(player1->score() + 1);
        winPlayer = player1;
        currentPlayer = player2;
        QMetaObject::invokeMethod(item, "setTurn", Q_ARG(QVariant, 2));
    }
    else {
        player2->setScore(player2->score() + 1);
        winPlayer = player2;
        currentPlayer = player1;
        QMetaObject::invokeMethod(item, "setTurn", Q_ARG(QVariant, 1));
    }

    // show message whose win, after 3 seconds hide it
    QMetaObject::invokeMethod(item, "showMessage", Q_ARG(QVariant, winPlayer->name() + "\nWIN!"));
    QTimer::singleShot(3000, this, [=](){
        QMetaObject::invokeMethod(item, "hideMessage");
        gameLogic->clear();
        QMetaObject::invokeMethod(item, "clearDesk");
    });
}

void NoughtAndCrossesModule::chooseSquare(int index, QObject *obj)
{
    // if clicked was on used area, ignore it
    if (! gameLogic->setFigure(currentPlayer->getFigure(), index))
        return;

    // draw turn on desk and swap players
    if (currentPlayer->getFigure() == Figure::Cross)
        QMetaObject::invokeMethod(obj, "setCross", Q_ARG(QVariant, index));
    else
        QMetaObject::invokeMethod(obj, "setNought", Q_ARG(QVariant, index));
    if (currentPlayer == player1){
        currentPlayer = player2;
        QMetaObject::invokeMethod(item, "setTurn", Q_ARG(QVariant, 2));
    }
    else {
        currentPlayer = player1;
        QMetaObject::invokeMethod(item, "setTurn", Q_ARG(QVariant, 1));
    }

    // check game for victory
    gameLogic->checkForVictory();
}

void NoughtAndCrossesModule::resetProgress()
{
    gameLogic->clear();
    player1->setScore(0);
    player2->setScore(0);
    QMetaObject::invokeMethod(item, "clearDesk");
}
