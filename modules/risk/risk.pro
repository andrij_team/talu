QT += qml
QT += quick

CONFIG += c++14
CONFIG += plugin
CONFIG += qtquickcompiler
DESTDIR = $$PWD/../../../TaLu_Bin/modules
TARGET = risk
TEMPLATE = lib

OTHER_FILES += $$PWD/QML/*.qml

HEADERS += \
    C++/riskmodule.h

SOURCES += \
    C++/riskmodule.cpp

RESOURCES += \
    risk_resource.qrc
