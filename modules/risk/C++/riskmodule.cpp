#include "riskmodule.h"

RiskModule::RiskModule()
{

}

void RiskModule::registrQmlProperty(){}

void RiskModule::closeModule(){}

QString RiskModule::getName()
{
    return "Risk";
}

QQuickItem* RiskModule::createGameItem(QQmlEngine *engine)
{
    QQmlComponent component(engine, QUrl("qrc:/risk/QML/main.qml"));
    qDebug() << component.errors() << component.isReady();
    return qobject_cast <QQuickItem*> (component.create());
}
